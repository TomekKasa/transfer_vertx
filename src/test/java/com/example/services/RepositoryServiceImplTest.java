package com.example.services;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryServiceImplTest {

    @Mock
    private SQLClient sqlClientMock;

    @Captor
    private ArgumentCaptor<Handler<AsyncResult<UpdateResult>>> updateResultCaptor;

    @Captor
    private ArgumentCaptor<Handler<AsyncResult<ResultSet>>> queryResultCaptor;

    @Captor
    private ArgumentCaptor<Handler<AsyncResult<SQLConnection>>> sqlConnectionResultCaptor;

    @Captor
    private ArgumentCaptor<Handler<AsyncResult<Void>>> voidResultCaptor;

    private RepositoryService repositoryService;

    private JsonObject account;
    private JsonObject transfer;

    private java.util.concurrent.CompletableFuture<Void> future;
    @Before
    public void setUp() {
        repositoryService = new RepositoryServiceImpl(sqlClientMock);
        account = new JsonObject()
            .put("id", 1)
            .put("balance", 0.0);

        transfer = new JsonObject()
            .put("toAccount", 1)
            .put("amount", 5.0);
        future = new CompletableFuture<>();
    }

    @After
    public void waitForAssertions() throws Exception {
        future.get(1, TimeUnit.SECONDS);
    }

    @Test
    public void createAccountSuccess() {
        repositoryService.createAccount(account, res-> { // Note this will be called at the end of test
            assertThat(res.succeeded(), is(true));
            assertThat(res.result(), equalTo(1));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).updateWithParams(
            eq("INSERT INTO Accounts VALUES(?,?)"),
            eq(new JsonArray().add(1).add(0.0)),
            updateResultCaptor.capture());
        updateResultCaptor.getValue().handle(Future.succeededFuture(new UpdateResult()));
    }

    @Test
    public void createAccountFailedDueToConflict() {
        repositoryService.createAccount(account, res -> {
            assertThat(res.failed(), is(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));

            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_CONFLICT));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).updateWithParams(
            eq("INSERT INTO Accounts VALUES(?,?)"),
            eq(new JsonArray().add(1).add(0.0)),
            updateResultCaptor.capture());
        // H2 Code for field uniqueness violation is 23505
        updateResultCaptor.getValue().handle(Future.failedFuture(new SQLException("Conflict", String.valueOf(23505), 23505)));
    }

    @Test
    public void createAccountFailedDueToOtherSQLException() {
        repositoryService.createAccount(account, res -> {
            assertThat(res.failed(), is(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));

            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            future.complete(null);
        });
        verify(sqlClientMock, times(1)).updateWithParams(
            eq("INSERT INTO Accounts VALUES(?,?)"),
            eq(new JsonArray().add(1).add(0.0)),
            updateResultCaptor.capture());
        // H2 Code for field uniqueness violation
        updateResultCaptor.getValue().handle(Future.failedFuture(new SQLException("Conflict", String.valueOf(23504), 23504)));
    }

    @Test
    public void createAccountFailedDueToOtherException() {
        repositoryService.createAccount(account, res -> {
            assertThat(res.failed(), is(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));

            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            future.complete(null);
        });
        verify(sqlClientMock, times(1)).updateWithParams(
            eq("INSERT INTO Accounts VALUES(?,?)"),
            eq(new JsonArray().add(1).add(0.0)),
            updateResultCaptor.capture());
        // H2 Code for field uniqueness violation
        updateResultCaptor.getValue().handle(Future.failedFuture(new Exception("Some Other Exception")));
    }

    @Test
    public void getAccountInfoSuccess() {
        Integer id = 1;
        Double balance = 10.0;

        repositoryService.getAccountInfo(id.toString(), res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonObject.class));
            assertThat(res.result(), equalTo(new JsonObject()
                .put("id", id)
                .put("balance", balance)));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).queryWithParams(
            eq("SELECT * FROM Accounts WHERE \"id\"=?"),
            eq(new JsonArray().add(id.toString())),
            queryResultCaptor.capture()
        );
        ResultSet resultSet = new ResultSet();
        resultSet.setResults(Collections.singletonList(new JsonArray().add(id).add(balance)));
        resultSet.setColumnNames(Arrays.asList("id", "balance"));
        queryResultCaptor.getValue().handle(Future.succeededFuture(resultSet));
    }

    @Test
    public void getAccountInfoAccountDoesNotExist() {
        Integer id = 1;
        repositoryService.getAccountInfo(id.toString(), res -> {
            assertThat(res.failed(), equalTo(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));

            ServiceException exception = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_NOT_FOUND));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).queryWithParams(
            eq("SELECT * FROM Accounts WHERE \"id\"=?"),
            eq(new JsonArray().add(id.toString())),
            queryResultCaptor.capture()
        );
        ResultSet resultSet = new ResultSet();
        resultSet.setResults(Collections.emptyList());
        queryResultCaptor.getValue().handle(Future.succeededFuture(resultSet));
    }

    @Test
    public void getAccountInfoFail() {
        Integer id = 1;
        repositoryService.getAccountInfo(id.toString(), res -> {
            assertThat(res.failed(), equalTo(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));

            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).queryWithParams(
            eq("SELECT * FROM Accounts WHERE \"id\"=?"),
            eq(new JsonArray().add(id.toString())),
            queryResultCaptor.capture()
        );
        queryResultCaptor.getValue().handle(Future.failedFuture(new SQLException("Some Exception", "Some SQL State", 123)));
    }

    @Test
    public void listAccountsSuccessEmptyList() {
        repositoryService.listAccounts( res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonArray.class));
            assertThat(res.result().size(), equalTo(0));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).query(
            eq("SELECT * FROM Accounts"),
            queryResultCaptor.capture()
        );

        ResultSet resultSet = new ResultSet();
        resultSet.setColumnNames(Collections.emptyList());
        resultSet.setResults(Collections.emptyList());
        queryResultCaptor.getValue().handle(Future.succeededFuture(resultSet));
    }

    @Test
    public void listAccountsSuccessListContainsOneElement() {
        Integer id = 1;
        Double balance = 5.0;

        repositoryService.listAccounts( res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonArray.class));
            assertThat(res.result().size(), equalTo(1));
            assertThat(res.result().getJsonObject(0), instanceOf(JsonObject.class));
            JsonObject account = res.result().getJsonObject(0);
            assertThat(account.getInteger("id"), equalTo(id));
            assertThat(account.getDouble("balance"), equalTo(balance));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).query(
            eq("SELECT * FROM Accounts"),
            queryResultCaptor.capture()
        );

        ResultSet resultSet = new ResultSet();
        resultSet.setColumnNames(Arrays.asList("id", "balance"));
        resultSet.setResults(Arrays.asList(
            new JsonArray().add(id).add(balance)
        ));
        queryResultCaptor.getValue().handle(Future.succeededFuture(resultSet));
    }

    @Test
    public void listAccountsFail() {
        repositoryService.listAccounts( res -> {
            assertThat(res.failed(), equalTo(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));
            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).query(
            eq("SELECT * FROM Accounts"),
            queryResultCaptor.capture()
        );

        queryResultCaptor.getValue().handle(Future.failedFuture(new SQLException("Some Reason", "Some State", 123)));
    }

    @Test
    public void listTransactionsSuccessEmptyList() {
        repositoryService.listTransactions(res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonArray.class));
            assertThat(res.result().size(), equalTo(0));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).query(
            eq("SELECT * FROM Transactions"),
            queryResultCaptor.capture()
        );

        ResultSet resultSet = new ResultSet();
        resultSet.setColumnNames(Collections.emptyList());
        resultSet.setResults(Collections.emptyList());
        queryResultCaptor.getValue().handle(Future.succeededFuture(resultSet));
    }

    @Test
    public void listTransactionsSuccessOneTransaction() {
        repositoryService.listTransactions(res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonArray.class));
            assertThat(res.result().size(), equalTo(1));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).query(
            eq("SELECT * FROM Transactions"),
            queryResultCaptor.capture()
        );

        ResultSet resultSet = new ResultSet();
        resultSet.setColumnNames(Arrays.asList("id", "fromAccount", "toAccount", "amount", "status", "message"));
        resultSet.setResults(Arrays.asList(
            new JsonArray().add(1).add(1).add(2).add(10.0).add(true).addNull()
        ));
        queryResultCaptor.getValue().handle(Future.succeededFuture(resultSet));
    }

    @Test
    public void listTransactionsFail() {
        repositoryService.listTransactions(res -> {
            assertThat(res.failed(), equalTo(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));
            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).query(
            eq("SELECT * FROM Transactions"),
            queryResultCaptor.capture()
        );

        queryResultCaptor.getValue().handle(Future.failedFuture(new SQLException("Some reason", "Some state", 1234)));
    }

    @Test
    public void executeTransactionAcquireConnectionFailed() {
        repositoryService.executeTransfer(transfer, res -> {
            assertThat(res.failed(), equalTo(true));
            assertThat(res.cause(), instanceOf(ServiceException.class));
            ServiceException exception  = (ServiceException)res.cause();
            assertThat(exception.failureCode(), equalTo(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            future.complete(null);
        });

        verify(sqlClientMock, times(1)).getConnection(sqlConnectionResultCaptor.capture());
        sqlConnectionResultCaptor.getValue().handle(Future.failedFuture(new SQLException("Some reason", "Some SQLState", 123)));
    }

    @Test
    public void executeTransactionTransactionStartFailed() {
        SQLConnection connection = Mockito.mock(SQLConnection.class);
        String reason = "Set AutoCommit failed";

        repositoryService.executeTransfer(transfer, res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonObject.class));
            JsonObject response = res.result();
            assertThat(response, equalTo(new JsonObject(transfer.encode())
                .put("status", false)
                .put("message", reason)
            ));
            future.complete(null);
        });


        verify(sqlClientMock, times(1)).getConnection(sqlConnectionResultCaptor.capture());
        sqlConnectionResultCaptor.getValue().handle(Future.succeededFuture(connection));

        verify(connection, times(1)).setAutoCommit(
            eq(false),
            voidResultCaptor.capture()
        );

        voidResultCaptor.getValue().handle(Future.failedFuture(new SQLException(reason, "SQL State", 1234)));

        verify(connection, times(1)).close(voidResultCaptor.capture());

        voidResultCaptor.getValue().handle(Future.succeededFuture(null));
    }

    @Test
    public void executeTransactionAddingToTargetAccountFailedFromAccountIsNull() {
        SQLConnection connection = Mockito.mock(SQLConnection.class);
        String reason = "Adding Failed";

        repositoryService.executeTransfer(transfer, res -> {
            assertThat(res.succeeded(), equalTo(true));
            assertThat(res.result(), instanceOf(JsonObject.class));
            JsonObject response = res.result();
            assertThat(response, equalTo(new JsonObject(transfer.encode())
                .put("status", false)
                .put("message", reason)
            ));
            future.complete(null);
        });


        verify(sqlClientMock, times(1)).getConnection(sqlConnectionResultCaptor.capture());
        sqlConnectionResultCaptor.getValue().handle(Future.succeededFuture(connection));

        verify(connection, times(1)).setAutoCommit(
            eq(false),
            voidResultCaptor.capture()
        );

        voidResultCaptor.getValue().handle(Future.succeededFuture(null));

        verify(connection, times(1)).updateWithParams(
            eq("UPDATE Accounts SET \"balance\"=\"balance\"+? WHERE \"id\"=?"),
            eq(new JsonArray().add(5.0).add(1)),
            updateResultCaptor.capture()
        );

        updateResultCaptor.getValue().handle(Future.failedFuture(reason));

        verify(connection, times(1)).rollback(voidResultCaptor.capture());
        voidResultCaptor.getValue().handle(Future.succeededFuture());

        verify(connection, times(1)).close(voidResultCaptor.capture());
        voidResultCaptor.getValue().handle(Future.succeededFuture(null));
    }
}
