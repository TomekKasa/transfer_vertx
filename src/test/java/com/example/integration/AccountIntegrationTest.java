package com.example.integration;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class AccountIntegrationTest extends BaseIntegrationTest{



    @Test
    public void addAccountSuccess(TestContext context) {
        Async async = context.async();
        addAccount(1,res -> {
            if (res.failed()) {
                context.fail(res.cause());
            }
            HttpResponse<Buffer> result = res.result();
            context.assertEquals(HttpStatus.SC_CREATED, result.statusCode());
            context.assertNull(result.body());
            context.assertTrue(result.headers().contains(HttpHeaders.LOCATION));
            async.countDown();
        });
    }

    @Test
    public void addAccountNoBody(TestContext context) {
        Async async = context.async();

        client
            .request(HttpMethod.PUT, ACCOUNTS_URI)
            .putHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType())
            .send(res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                }

                HttpResponse<Buffer> response = res.result();
                context.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
                async.countDown();
            });
    }

    @Test
    public void addAccountEmptyBody(TestContext context) {
        Async async = context.async();

        client
            .request(HttpMethod.PUT, ACCOUNTS_URI)
            .sendJsonObject(new JsonObject(), res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                }

                HttpResponse<Buffer> response = res.result();
                context.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

                JsonObject responseBody = response.bodyAsJsonObject();
                context.assertEquals(new JsonObject()
                    .put("code", HttpStatus.SC_BAD_REQUEST)
                    .put("message", "$.id: is missing but it is required"), responseBody);
                async.countDown();
            });
    }

    @Test
    public void addExistingAccountFails(TestContext context) {
        Async async = context.async(2);
        addAccount(1, res -> {
            if (res.failed()) {
                context.fail(res.cause());
                return;
            }
            HttpResponse<Buffer> result = res.result();
            context.assertEquals(HttpStatus.SC_CREATED, result.statusCode());
            context.assertNull(result.body());
            context.assertTrue(result.headers().contains(HttpHeaders.LOCATION));
            async.countDown();
        });

        addAccount(1, res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                    return;
                }

                HttpResponse<Buffer> result = res.result();
                context.assertEquals(HttpStatus.SC_CONFLICT, result.statusCode());
                async.countDown();
            });
    }

    @Test
    public void getAccountInfoSuccess(TestContext context) {
        Async async = context.async();

        addAccount(1, res -> async.countDown());
        getAccountInfo(1, res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                    return;
                }

                HttpResponse<Buffer> response = res.result();
                context.assertEquals(HttpStatus.SC_OK, response.statusCode());
            JsonObject responseBody = response.bodyAsJsonObject();
            context.assertEquals(1, responseBody.getInteger("id"));
            context.assertEquals(0.0, responseBody.getDouble("balance"));
            async.countDown();
            });
    }

    @Test
    public void getAccountInfoNotFound(TestContext context) {
        Async async = context.async();

        getAccountInfo(1, res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                    return;
                }

                HttpResponse<Buffer> response = res.result();
                context.assertEquals(HttpStatus.SC_NOT_FOUND, response.statusCode());
                async.countDown();
            });
    }

    @Test
    public void listAccountsEmpty(TestContext context) {
        Async async = context.async();

        client
            .request(HttpMethod.GET, ACCOUNTS_URI)
            .send( res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                }

                HttpResponse<Buffer> result = res.result();
                context.assertEquals(HttpStatus.SC_OK, result.statusCode());
                JsonArray accounts = res.result().bodyAsJsonArray();
                context.assertEquals(0, accounts.size());
                async.countDown();
            });
    }

    @Test
    public void listAccountsAfterAddingAccount(TestContext context) {
        Async async = context.async(2);

        addAccount(1, res -> {
            if (res.failed()) {
                context.fail(res.cause());
                return;
            }
            async.countDown();
        });

        client
            .request(HttpMethod.GET, ACCOUNTS_URI)
            .send( res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                    return;
                }

                HttpResponse<Buffer> result = res.result();
                context.assertEquals(HttpStatus.SC_OK, result.statusCode());
                JsonArray accounts = res.result().bodyAsJsonArray();
                context.assertEquals(1, accounts.size());
                async.countDown();
            });
    }

}
