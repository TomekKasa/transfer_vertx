package com.example.integration;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class TransfersIntegrationTests extends BaseIntegrationTest {

    @Test
    public void executeSuccessfulTransfer(TestContext context) {
        Async async = context.async(4);

        addAccount(1, 10.0, res -> async.countDown());
        addAccount(2, 10.0, res -> async.countDown());

        excecuteTransfer(1, 2, 5.0, res -> {
            if (res.failed()) {
                context.fail(res.cause());
                return;
            }

            HttpResponse<Buffer> response = res.result();
            context.assertEquals(HttpStatus.SC_OK, response.statusCode());
            JsonObject responseBody = response.bodyAsJsonObject();

            context.assertEquals(true, responseBody.getBoolean("status"));

            getAccountInfo(1, resAccount1 -> {
                if (resAccount1.failed()) {
                    context.fail(resAccount1.cause());
                    return;
                }
                JsonObject account1 = resAccount1.result().bodyAsJsonObject();
                context.assertEquals(5.0, account1.getDouble("balance"));
                async.countDown();
            });

            getAccountInfo(2, resAccount2 -> {
                if (resAccount2.failed()) {
                    context.fail(resAccount2.cause());
                    return;
                }
                JsonObject account2 = resAccount2.result().bodyAsJsonObject();
                context.assertEquals(15.0, account2.getDouble("balance"));
                async.countDown();
            });
        });
    }

    @Test
    public void executeTransferInsufficientFunds(TestContext context) {
        Async async = context.async(4);

        addAccount(1, 10.0, res -> async.countDown());
        addAccount(2, 10.0, res -> async.countDown());

        excecuteTransfer(1, 2, 15.0, res -> {
            if (res.failed()) {
                context.fail(res.cause());
                return;
            }

            HttpResponse<Buffer> response = res.result();
            context.assertEquals(HttpStatus.SC_OK, response.statusCode());
            JsonObject responseBody = response.bodyAsJsonObject();

            context.assertEquals(false, responseBody.getBoolean("status"));

            getAccountInfo(1, resAccount1 -> {
                if (resAccount1.failed()) {
                    context.fail(resAccount1.cause());
                    return;
                }
                JsonObject account1 = resAccount1.result().bodyAsJsonObject();
                context.assertEquals(10.0, account1.getDouble("balance"));
                async.countDown();
            });

            getAccountInfo(2, resAccount2 -> {
                if (resAccount2.failed()) {
                    context.fail(resAccount2.cause());
                    return;
                }
                JsonObject account2 = resAccount2.result().bodyAsJsonObject();
                context.assertEquals(10.0, account2.getDouble("balance"));
                async.countDown();
            });
        });
    }

    @Test
    public void executeTransferFromAccountDoesNotExist(TestContext context) {
        Async async = context.async(2);

        addAccount(2, 10.0, res -> async.countDown());

        excecuteTransfer(1, 2, 5.0, res -> {
            if (res.failed()) {
                context.fail(res.cause());
                return;
            }

            HttpResponse<Buffer> response = res.result();
            context.assertEquals(HttpStatus.SC_OK, response.statusCode());
            JsonObject responseBody = response.bodyAsJsonObject();

            context.assertEquals(false, responseBody.getBoolean("status"));

            getAccountInfo(2, resAccount2 -> {
                if (resAccount2.failed()) {
                    context.fail(resAccount2.cause());
                    return;
                }
                JsonObject account2 = resAccount2.result().bodyAsJsonObject();
                context.assertEquals(10.0, account2.getDouble("balance"));
                async.countDown();
            });
        });
    }

    @Test
    public void executeDeposit(TestContext context) {
        Async async = context.async(2);
        addAccount(1, 10.0, res -> async.countDown());

        excecuteTransfer(null, 1, 15.0, res -> {
            if (res.failed()) {
                context.fail(res.cause());
                return;
            }

            HttpResponse<Buffer> response = res.result();
            System.out.println(response.bodyAsJsonObject().encodePrettily());
            context.assertEquals(HttpStatus.SC_OK, response.statusCode());
            JsonObject responseBody = response.bodyAsJsonObject();

            context.assertEquals(true, responseBody.getBoolean("status"));

            getAccountInfo(1, resAccount1 -> {
                if (resAccount1.failed()) {
                    context.fail(resAccount1.cause());
                    return;
                }
                JsonObject account1 = resAccount1.result().bodyAsJsonObject();
                context.assertEquals(25.0, account1.getDouble("balance"));
                async.countDown();
            });
        });
    }

    @Test
    public void getTransfersEmptyList(TestContext context) {
        Async async = context.async();

        client
            .request(HttpMethod.GET, TRANSFERS_URI)
            .send(res -> {
                if (res.failed()) {
                    context.fail(res.cause());
                    return;
                }
                HttpResponse<Buffer> response = res.result();
                context.assertEquals(HttpStatus.SC_OK, response.statusCode());

                JsonArray responseBody = response.bodyAsJsonArray();
                context.assertEquals(0, responseBody.size());
                async.countDown();
            });
    }
}
