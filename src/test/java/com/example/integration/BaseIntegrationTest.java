package com.example.integration;

import com.example.MainVerticle;
import com.sun.xml.internal.ws.client.ResponseContext;
import io.swagger.models.auth.In;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

import java.io.IOException;
import java.net.ServerSocket;

public class BaseIntegrationTest {
    final String ACCOUNTS_URI = "/accounts";
    final String TRANSFERS_URI = "/transfers";

    private int port;
    WebClient client;

    @Rule
    public RunTestOnContext rule = new RunTestOnContext();

    @Before
    public void setupVertx(TestContext context) throws IOException {
        rule.vertx().exceptionHandler(context.exceptionHandler());
        ServerSocket socket = new ServerSocket(0);
        port = socket.getLocalPort();
        socket.close();

        DeploymentOptions options = new DeploymentOptions()
            .setConfig(new JsonObject().put("http.port", port));

        rule.vertx().deployVerticle(MainVerticle.class.getName(), options, context.asyncAssertSuccess());

        client = WebClient.create(rule.vertx(), new WebClientOptions().setDefaultPort(port));
    }

    @After
    public void close(TestContext context) {
        rule.vertx().close(context.asyncAssertSuccess());
    }

    void addAccount(Integer id, Handler<AsyncResult<HttpResponse<Buffer>>> handler) {
        addAccount(id, 0.0, handler);
    }

    void addAccount(Integer id, Double balance, Handler<AsyncResult<HttpResponse<Buffer>>> handler) {
        JsonObject body = new JsonObject()
            .put("id", id)
            .put("balance", balance);
        client
            .request(HttpMethod.PUT, ACCOUNTS_URI)
            .sendJsonObject(body, handler);
    }

    void getAccountInfo(Integer id, Handler<AsyncResult<HttpResponse<Buffer>>> handler) {
        client
            .request(HttpMethod.GET, ACCOUNTS_URI + "/" + id)
            .send(handler);
    }

    void excecuteTransfer(Integer fromAccount, Integer toAccount, Double amount, Handler<AsyncResult<HttpResponse<Buffer>>> handler ) {
        JsonObject body = new JsonObject()
            .put("toAccount", toAccount)
            .put("amount", amount);
        if (fromAccount != null) {
            body.put("fromAccount", fromAccount);
        }

        client
            .request(HttpMethod.POST, TRANSFERS_URI)
            .sendJsonObject(body, handler);
    }
}
