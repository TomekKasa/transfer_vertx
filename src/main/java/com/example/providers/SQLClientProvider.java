package com.example.providers;

import com.example.services.RepositoryService;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;

@Slf4j
public class SQLClientProvider {
    private static final String CREATE_TABLE_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS ";

    public static SQLClient provideSQLClient(Vertx vertx) {
        return JDBCClient.createShared(vertx, new JsonObject()
            .put("url", "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1")
            .put("driver_class", "org.h2.Driver")
            .put("max_pool_size", 30)
            .put("user", "SA")
            .put("password", ""));
    }

    public static void initializeDatabase(Vertx vertx) {
        SQLClient sqlClient = SQLClientProvider.provideSQLClient(vertx);

        sqlClient.getConnection( connResult -> {
            if (connResult.failed()) {
                throw new RuntimeException("Unable to Get DB Connection");
            }

            SQLConnection connection = connResult.result();
            connection.batch(
                Arrays.asList(
                    dropTable(RepositoryService.ACCOUNTS_TABLE_NAME),
                    createAccountsTable(),
                    dropTable(RepositoryService.TRANSACTIONS_TABLE_NAME),
                    createTransactionTable()
                ), res -> {
                    if (res.failed()) {
                        throw new RuntimeException("Unable to create databases.");
                    }
                    log.info("Databases Initialized");
                }
            );
        });
    }

    private static String createTransactionTable() {
        StringBuilder createTransactionsQueryBuilder = new StringBuilder();
        createTransactionsQueryBuilder
            .append(CREATE_TABLE_IF_NOT_EXISTS)
            .append(RepositoryService.TRANSACTIONS_TABLE_NAME)
            .append("(")
            .append("\"id\" IDENTITY PRIMARY KEY,")
            .append("\"fromAccount\" INT NULL,")
            .append("\"toAccount\" INT,")
            .append("\"status\" BOOLEAN,")
            .append("\"message\" VARCHAR(255) NULL")
            .append(")");
        return createTransactionsQueryBuilder.toString();
    }

    private static String createAccountsTable() {
        StringBuilder createAccountsQueryBuilder = new StringBuilder();
        createAccountsQueryBuilder
            .append(CREATE_TABLE_IF_NOT_EXISTS)
            .append(RepositoryService.ACCOUNTS_TABLE_NAME)
            .append("(")
            .append("\"id\" INT PRIMARY KEY,")
            .append("\"balance\" DECIMAL(19,4) DEFAULT 0.0 CHECK(\"balance\">=0.0)")
            .append(")");
        return createAccountsQueryBuilder.toString();
    }

    private static String dropTable(String tableName) {
        return "DROP TABLE IF EXISTS " + tableName;
    }
}
