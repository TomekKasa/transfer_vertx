package com.example.services;

import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.serviceproxy.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import java.sql.SQLException;

import static io.vertx.core.Future.failedFuture;
import static io.vertx.core.Future.succeededFuture;

@Slf4j
public class RepositoryServiceImpl implements RepositoryService {
    private static final String SELECT_ACCOUNTS_QUERY = "SELECT * FROM " + ACCOUNTS_TABLE_NAME;
    private static final String SELECT_ACCOUNT_QUERY = SELECT_ACCOUNTS_QUERY + " WHERE \"id\"=?";
    private static final String INSERT_ACCOUNT_QUERY = "INSERT INTO " + ACCOUNTS_TABLE_NAME + " VALUES(?,?)";
    private static final String UPDATE_ACCOUNT_QUERY = "UPDATE " + ACCOUNTS_TABLE_NAME + " SET \"balance\"=\"balance\"";
    private static final String ADD_ACCOUNT_BALANCE = UPDATE_ACCOUNT_QUERY + "+? WHERE \"id\"=?";
    private static final String SUBTRACT_ACCOUNT_BALANCE = UPDATE_ACCOUNT_QUERY + "-? WHERE \"id\"=?";
    private final SQLClient sqlClient;

    public RepositoryServiceImpl(SQLClient client) {
        this.sqlClient = client;
    }

    @Override
    public void createAccount(JsonObject accountData, Handler<AsyncResult<Integer>> handler) {
        log.debug("createAccount Start, account = {}", accountData);
        Integer accountId = accountData.getInteger("id");
        JsonArray params = new JsonArray()
            .add(accountId)
            .add(accountData.getDouble("balance", 0.0));

        sqlClient.updateWithParams(INSERT_ACCOUNT_QUERY, params, res -> handleCreateAccountDBResponse(res, accountId, handler));
    }

    @Override
    public void getAccountInfo(String id, Handler<AsyncResult<JsonObject>> handler) {
        log.debug("getAccount Start. id = {}", id);
        sqlClient.queryWithParams(SELECT_ACCOUNT_QUERY, new JsonArray().add(id), res -> handleGetAccountDBResponse(res, handler));
    }

    @Override
    public void listAccounts(Handler<AsyncResult<JsonArray>> handler) {
        log.debug("listAccounts Start");
        sqlClient.query(SELECT_ACCOUNTS_QUERY, res -> {
            log.debug("listAccounts Result Status = {}", res.failed());
            if (res.failed()) {
                handleInternalError(res.cause(), handler);
            } else {
                log.debug("listAccounts query success");
                handler.handle(succeededFuture(new JsonArray(res.result().getRows())));
            }
        });
    }

    @Override
    public void listTransactions(Handler<AsyncResult<JsonArray>> handler) {
        sqlClient.query("SELECT * FROM " + TRANSACTIONS_TABLE_NAME, res -> {
            if (res.failed()) {
                handleInternalError(res.cause(), handler);
            } else {
                handler.handle(succeededFuture(new JsonArray(res.result().getRows())));
            }
        });
    }

    @Override
    public void executeTransfer(JsonObject transferJson, Handler<AsyncResult<JsonObject>> mainHandler) {
        log.debug("executeTransfer Start. Transaction = {}", transferJson);
        acquireConnection(transferJson, mainHandler, connection ->
            startTransaction(transferJson, mainHandler, connection, ignored1 ->
                applyUpdates(transferJson, mainHandler, connection, applyResult ->
                    handleApplyResults(transferJson, mainHandler, connection, applyResult, ignored2 ->
                        closeConnection(transferJson, mainHandler, connection, null)))));
    }

    private void acquireConnection(JsonObject transactionJson,
                                   Handler<AsyncResult<JsonObject>> mainHandler,
                                   Handler<SQLConnection> handler) {
        sqlClient.getConnection( res -> {
            if (res.failed()) {
                log.error("[T : {}] : Failed to acquire connection", transactionJson);
                handleInternalError(res.cause(), mainHandler);
            } else {
                log.debug("[T : {}] : Connection acquired", transactionJson);
                handler.handle(res.result());
            }
        });
    }

    private void startTransaction(JsonObject transactionJson,
                                  Handler<AsyncResult<JsonObject>> mainHandler,
                                  SQLConnection connection,
                                  Handler<Void> next) {
        connection.setAutoCommit(false, res -> {
            if (res.failed()) {
                log.error("[T: {}] : Failed to start transaction", transactionJson);
                closeConnection(transactionJson, mainHandler, connection, res.cause());
                return;
            }
            log.debug("[T: {}] : Transaction started", transactionJson);
            next.handle(null);
        });
    }

    void applyUpdates(JsonObject transactionJson,
                      Handler<AsyncResult<JsonObject>> mainHandler,
                      SQLConnection connection,
                      Handler<AsyncResult<CompositeFuture>> handler) {
        Integer fromAccount = transactionJson.getInteger("fromAccount", null);
        Integer toAccount = transactionJson.getInteger("toAccount");
        Double amount = transactionJson.getDouble("amount");

        CompositeFuture.all(
            addAmountToAccount(connection, toAccount, amount),
            subtractAmountFromAccount(connection, fromAccount, amount)
        ).setHandler( handler );
    }

    void handleApplyResults(JsonObject transactionJson,
                            Handler<AsyncResult<JsonObject>> mainHandler,
                            SQLConnection connection,
                            AsyncResult<CompositeFuture> applyResult,
                            Handler<Void> next) {
        if (applyResult.failed()) {
            log.error("[T : {}]. Apply updates failed. {}", transactionJson, applyResult.cause().getMessage());
            rollback(transactionJson, mainHandler, connection, applyResult.cause());
            return;
        }

        commit(transactionJson, mainHandler, connection, next);
    }

    private void rollback(JsonObject transactionJson,
                          Handler<AsyncResult<JsonObject>> mainHandler,
                          SQLConnection connection,
                          Throwable rollbackCause) {
        connection.rollback( res -> {
            if (res.failed()) {
                log.error("[T : {}]. Rollback failed. {}", transactionJson, res.cause().getMessage());
                closeConnection(transactionJson, mainHandler, connection, res.cause());
            } else {
                log.debug("[T : {}]. Rollback succeeded.", transactionJson);
                closeConnection(transactionJson, mainHandler, connection, rollbackCause);
            }
        });
    }

    private void commit(JsonObject transactionJson,
                        Handler<AsyncResult<JsonObject>> mainHandler,
                        SQLConnection connection,
                        Handler<Void> next) {
        connection.commit( res -> {
            if (res.failed()) {
                log.error("[T : {}] : Commit failed. {}", res.cause());
                closeConnection(transactionJson,mainHandler,connection,res.cause());
            } else {
                log.debug("[T : {}] : Commit success. {}");
                next.handle(null);
            }
        });
    }

    private void closeConnection(JsonObject transactionJson,
                                 Handler<AsyncResult<JsonObject>> mainHandler,
                                 SQLConnection connection,
                                 Throwable cause) {
        connection.close( res -> {
            if (res.failed()) {
                log.error("[T : {}] : Failed to close connection. {}", res.cause());
                handleInternalError(res.cause(), mainHandler);
                return;
            }
            transactionStatusHandler(transactionJson, cause, mainHandler);
        });
    }
    private Future<Void> addAmountToAccount(SQLConnection connection, Integer toAccount, Double amount) {
        return updateBalance(connection, ADD_ACCOUNT_BALANCE, toAccount, amount);
    }

    private Future<Void> subtractAmountFromAccount(SQLConnection connection, Integer fromAccount, Double amount) {
        if (fromAccount == null) {
            return Future.succeededFuture();
        }
        return updateBalance(connection, SUBTRACT_ACCOUNT_BALANCE, fromAccount, amount);
    }


    private Future<Void> updateBalance(SQLConnection connection, String query, Integer toAccount, Double amount) {
        Future<Void> done = Future.future();
        JsonArray addParams = new JsonArray()
            .add(amount)
            .add(toAccount);

        connection.updateWithParams(query, addParams, addRes -> {
           if (addRes.failed()) {
               log.warn("Update Balance {} to {} failed. Rollback. {}", amount, toAccount, addRes.cause().getMessage());
               done.fail(addRes.cause());
           } else if (addRes.result().getUpdated() == 0) {
               log.warn("Update Balance {} to {} failed. Rollback", amount, toAccount);
               done.fail(new ServiceException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Update failed"));
           } else {
               log.debug("Update Success");
               done.complete();
           }
        });
        return done;
    }

    private <T> void handleError(String message, int code, Handler<AsyncResult<T>> handler) {
        handler.handle(failedFuture(new ServiceException(code, message)));
    }

    private <T> void handleConflict(Handler<AsyncResult<T>> handler) {
        handleError("Account already exists", HttpStatus.SC_CONFLICT, handler);
    }

    private <T> void handleInternalError(Throwable cause, Handler<AsyncResult<T>> handler) {
        log.error("Internal Error. {}", cause.getMessage());
        handleError(cause.getMessage(), HttpStatus.SC_INTERNAL_SERVER_ERROR, handler);
    }

    private void transactionStatusHandler(JsonObject transaction, Throwable cause, Handler<AsyncResult<JsonObject>> handler) {
        transaction.put("status", cause == null);
        Integer fromAccount = transaction.getInteger("fromAccount", null);
        JsonArray params = new JsonArray();
        if (fromAccount != null)
            params.add(fromAccount);
        else {
            params.addNull();
        }
        params
            .add(transaction.getInteger("toAccount"))
            .add(transaction.getDouble("amount"))
            .add(transaction.getBoolean("status"));
        if (cause != null) {
            transaction.put("message", cause.getMessage());
            params.add(cause.getMessage());
        } else {
            params.addNull();
        }

        sqlClient.queryWithParams("INSERT INTO Transactions VALUES(?,?,?,?,?)", params, ignoredRes -> {});
        handler.handle(succeededFuture(transaction));
    }

    private void handleCreateAccountDBResponse(AsyncResult<UpdateResult> res, Integer accountId, Handler<AsyncResult<Integer>> handler) {
        if (res.failed()) {
            if (isPrimaryIndexConstraintViolation(res.cause())) {
                log.info("Create Failed Due to conflict. accountId = {}", accountId);
                handleConflict(handler);
            } else {
                handleInternalError(res.cause(), handler);
            }
        } else {
            log.debug("createAccount OK.");
            handler.handle(succeededFuture(accountId));
        }
    }

    private boolean isPrimaryIndexConstraintViolation(Throwable cause) {
        final int primaryIndexConstraintViolation = 23505;
        if (cause instanceof SQLException) {
            SQLException ex = (SQLException)cause;
            return (ex.getErrorCode() == primaryIndexConstraintViolation);
        }
        return false;
    }

    private void handleGetAccountDBResponse(AsyncResult<ResultSet> res, Handler<AsyncResult<JsonObject>> handler) {
        if (res.failed()) {
            handleInternalError(res.cause(), handler);
        } else if (res.result().getNumRows() == 0) {
            log.debug("Account not found");
            handler.handle(failedFuture(new ServiceException(HttpStatus.SC_NOT_FOUND, "Resource not found")));
        } else {
            JsonObject accountInfo = getAccountInfoFromDBResponse(res.result());
            log.debug("Account found: {}", accountInfo);
            handler.handle(succeededFuture(accountInfo));
        }
    }

    private JsonObject getAccountInfoFromDBResponse(ResultSet resultSet) {
        return resultSet.getRows(true).get(0);
    }
}
