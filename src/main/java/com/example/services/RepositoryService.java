package com.example.services;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;


@ProxyGen
public interface RepositoryService {
    String EVENT_BUS_ADDRESS = "database-service-address";
    String ACCOUNTS_TABLE_NAME = "Accounts";
    String TRANSACTIONS_TABLE_NAME = "Transactions";

    void createAccount(JsonObject accountData, Handler<AsyncResult<Integer>> handler);
    void getAccountInfo(String id, Handler<AsyncResult<JsonObject>> handler);
    void listAccounts(Handler<AsyncResult<JsonArray>> handler);

    void executeTransfer(JsonObject transfer, Handler<AsyncResult<JsonObject>> handler);
    void listTransactions(Handler<AsyncResult<JsonArray>> handler);
}
