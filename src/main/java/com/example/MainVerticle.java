package com.example;

import com.example.providers.SQLClientProvider;
import com.example.services.RepositoryService;
import com.example.verticles.DatabaseWorkerVerticle;
import com.example.verticles.ServerVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainVerticle extends AbstractVerticle {

    private static final int DEFAULT_WORKER_POOL_SIZE = 1;
    private static final String CONFIG_WORKER_POOL_SIZE = "worker.pool.size";

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        initializeDatabase();
    }

    @Override
    public void start(Future<Void> done) throws Exception {
        CompositeFuture.all(
            deployServer(),
            deployDatabaseWorker()
        ).setHandler(result -> {
            if (result.succeeded()) {
                log.info("Successfully deployed com.example.verticles");
                done.complete();
            } else {
                log.info("Unable to deploy com.example.verticles. {}", result.cause().getMessage());
                done.fail(result.cause());
                vertx.close();
            }
        });
    }

    private Future<Void> deployServer() {
        DeploymentOptions serverOpts = new DeploymentOptions()
            .setWorker(false)
            .setInstances(config().getInteger(CONFIG_WORKER_POOL_SIZE, DEFAULT_WORKER_POOL_SIZE))
            .setWorkerPoolName("server-worker")
            .setWorkerPoolSize(config().getInteger(CONFIG_WORKER_POOL_SIZE, DEFAULT_WORKER_POOL_SIZE))
            .setConfig(config());
        return deploy(ServerVerticle.class.getName(), serverOpts);
    }

    private Future<Void> deployDatabaseWorker() {
        DeploymentOptions workerOpts = new DeploymentOptions()
            .setWorker(true)
            .setWorkerPoolSize(config().getInteger(CONFIG_WORKER_POOL_SIZE, DEFAULT_WORKER_POOL_SIZE))
            .setWorkerPoolName("database-worker")
            .setConfig(config());
        return deploy(DatabaseWorkerVerticle.class.getName(), workerOpts);
    }


    private Future<Void> deploy(String name, DeploymentOptions opts){
        Future<Void> done = Future.future();
        vertx.deployVerticle(name, opts, res -> {
            if(res.failed()){
                log.error("Failed to deploy verticle " + name);
                done.fail(res.cause());
            }
            else {
                log.info("Deployed verticle " + name);
                done.complete();
            }
        });

        return done;
    }

    private void initializeDatabase() {
        SQLClientProvider.initializeDatabase(vertx);
    }
}
