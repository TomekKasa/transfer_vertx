package com.example.handlers;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.validation.ValidationException;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.impl.EnglishReasonPhraseCatalog;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;

public enum CustomFailureHandler implements Handler<RoutingContext> {
    INSTANCE;

    @Override
    public void handle(RoutingContext routingContext) {
        if (routingContext.failure() instanceof ValidationException) {
            routingContext
                .response()
                .setStatusCode(SC_BAD_REQUEST)
                .setStatusMessage(EnglishReasonPhraseCatalog.INSTANCE.getReason(SC_BAD_REQUEST, null))
                .end(new JsonObject().put("code",SC_BAD_REQUEST).put("message", routingContext.failure().getMessage()).encodePrettily());
        } else if (routingContext.failure() instanceof ServiceException){
            ServiceException ex = (ServiceException)routingContext.failure();
            routingContext
                .response()
                .setStatusCode(ex.failureCode())
                .setStatusMessage(EnglishReasonPhraseCatalog.INSTANCE.getReason(ex.failureCode(), null))
                .end(new JsonObject().put("code", ex.failureCode()).put("message", ex.getMessage()).encodePrettily());
        } else {
            routingContext
                .response()
                .setStatusCode(SC_INTERNAL_SERVER_ERROR)
                .setStatusMessage(EnglishReasonPhraseCatalog.INSTANCE.getReason(SC_INTERNAL_SERVER_ERROR, null))
                .end(new JsonObject().put("code", SC_INTERNAL_SERVER_ERROR).put("message", routingContext.failure().getMessage()).encodePrettily());
        }
    }
}
