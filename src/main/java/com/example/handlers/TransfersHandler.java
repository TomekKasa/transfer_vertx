package com.example.handlers;

import io.vertx.ext.web.RoutingContext;

public interface TransfersHandler {
    void getTransfers(RoutingContext context);
    void executeTransfer(RoutingContext context);
}
