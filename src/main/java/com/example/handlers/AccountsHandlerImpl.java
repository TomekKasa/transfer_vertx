package com.example.handlers;

import com.example.services.RepositoryService;
import io.vertx.ext.web.RoutingContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;

@Slf4j
public class AccountsHandlerImpl implements AccountsHandler {
    private final RepositoryService repositoryService;
    public AccountsHandlerImpl(RepositoryService service) {
        this.repositoryService = service;
    }

    @Override
    public void addAccount(RoutingContext ctx) {
        repositoryService.createAccount(ctx.getBodyAsJson(), result -> {
            if (result.failed()) {
                ctx.fail(result.cause());
            } else {
                log.info("Successfully added account {}", ctx.getBodyAsJson());
                ctx.response()
                    .setStatusCode(HttpStatus.SC_CREATED)
                    .putHeader(HttpHeaders.LOCATION, ctx.request().absoluteURI() + "/" + result.result())
                    .end();
            }
        });
    }

    @Override
    public void getAccountInfo(RoutingContext ctx) {
        log.debug("getAccount Start");
        repositoryService.getAccountInfo(ctx.pathParam("account"), result -> {
            log.debug("getAccount Result failed = {}", result.failed());
            if (result.failed()) {
                ctx.fail(result.cause());
            } else {
                ctx
                    .response()
                    .end(result.result().encodePrettily());
            }
        });
    }

    @Override
    public void getAccounts(RoutingContext ctx) {
        log.debug("getAccounts start");
        repositoryService.listAccounts(result -> {
            if (result.failed()) {
                ctx.fail(result.cause());
            } else {
                log.info("Successfully retrieved account list. Size = {}", result.result().size());
                ctx
                    .response()
                    .end(result.result().encodePrettily());
            }
        });
    }
}
