package com.example.handlers;

import com.example.services.RepositoryService;
import io.vertx.ext.web.RoutingContext;

public class  TransfersHandlerImpl implements TransfersHandler {
    private final RepositoryService repositoryService;

    public TransfersHandlerImpl(RepositoryService service) {
        this.repositoryService = service;
    }

    @Override
    public void getTransfers(RoutingContext context) {
        repositoryService.listTransactions(result -> {
            if (result.failed()) {
                context.fail(result.cause());
            } else {
                context
                    .response()
                    .end(result.result().encodePrettily());
            }
        });
    }

    @Override
    public void executeTransfer(RoutingContext context) {
        repositoryService.executeTransfer(context.getBodyAsJson(), result -> {
            if (result.failed()) {
                context.fail(result.cause());
            } else {
                context
                    .response()
                    .end(result.result().encodePrettily());
            }
        });
    }
}
