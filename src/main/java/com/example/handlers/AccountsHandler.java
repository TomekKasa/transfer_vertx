package com.example.handlers;

import io.vertx.ext.web.RoutingContext;

public interface AccountsHandler {
    void addAccount(RoutingContext ctx);
    void getAccountInfo(RoutingContext ctx);
    void getAccounts(RoutingContext ctx);
}
