package com.example.verticles;


import com.example.handlers.AccountsHandler;
import com.example.handlers.AccountsHandlerImpl;
import com.example.handlers.CustomFailureHandler;
import com.example.handlers.TransfersHandler;
import com.example.handlers.TransfersHandlerImpl;
import com.example.services.RepositoryService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.serviceproxy.ServiceProxyBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServerVerticle extends AbstractVerticle {
    private static final String CONFIG_HTTP_PORT = "http.port";
    private static final Integer DEFAULT_HTTP_PORT = 8080;
    private static final String OPENAPI_PATH = "/api.yaml";

    private AccountsHandler accountsHandler;
    private TransfersHandler transfersHandler;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        ServiceProxyBuilder builder = new ServiceProxyBuilder(vertx).setAddress(RepositoryService.EVENT_BUS_ADDRESS);
        RepositoryService repositoryService = builder.build(RepositoryService.class);

        accountsHandler = new AccountsHandlerImpl(repositoryService);
        transfersHandler = new TransfersHandlerImpl(repositoryService);

        initializeRouter( routerAsyncResult ->
            startHTTPServer(routerAsyncResult, startFuture));
    }

    private void startHTTPServer(AsyncResult<Router> routerAsyncResult, Future<Void> startFuture) {
        int port = config().getInteger(CONFIG_HTTP_PORT, DEFAULT_HTTP_PORT);
        if (routerAsyncResult.failed()) {
            throw new RuntimeException(routerAsyncResult.cause());
        }
        Router router = routerAsyncResult.result();
        vertx.createHttpServer().requestHandler(router::accept).listen(port, result -> {
            if (result.failed()) {
                startFuture.fail(result.cause());
            } else {
                log.info("HTTP server started on port {}", port);
                startFuture.complete();
            }
        });
    }

    private void initializeRouter(Handler<AsyncResult<Router>> handler) {
        OpenAPI3RouterFactory.create(vertx, OPENAPI_PATH, createResult -> {
            if (createResult.failed()) {
                throw new RuntimeException(createResult.cause());
            }

            OpenAPI3RouterFactory factory = createResult.result();
            factory.setOptions(new RouterFactoryOptions()
                .setBodyHandler(BodyHandler.create().setBodyLimit(1000))
                .setMountNotImplementedHandler(true)
                .setValidationFailureHandler(CustomFailureHandler.INSTANCE)
                .setMountResponseContentTypeHandler(true)
                .setMountValidationFailureHandler(true));


            factory.addHandlerByOperationId("createAccount", accountsHandler::addAccount);
            factory.addHandlerByOperationId("getAccountInfo", accountsHandler::getAccountInfo);
            factory.addHandlerByOperationId("getAccounts", accountsHandler::getAccounts);

            factory.addHandlerByOperationId("getTransfers", transfersHandler::getTransfers);
            factory.addHandlerByOperationId("executeTransfer", transfersHandler::executeTransfer);

            handler.handle(Future.succeededFuture(factory.getRouter()));
        });
    }
}
