package com.example.verticles;

import com.example.providers.SQLClientProvider;
import com.example.services.RepositoryService;
import com.example.services.RepositoryServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import lombok.extern.slf4j.Slf4j;

@Slf4j

public class DatabaseWorkerVerticle extends AbstractVerticle {

    private RepositoryService repositoryService;

    @Override
    public void init(Vertx vertx, Context context)
    {
        super.init(vertx, context);
        repositoryService = new RepositoryServiceImpl(SQLClientProvider.provideSQLClient(vertx));
    }

    @Override
    public void start(Future<Void> startFuture) {
        log.info("Database Verticle Started");
        MessageConsumer<JsonObject> binder = new ServiceBinder(vertx)
            .setAddress(RepositoryService.EVENT_BUS_ADDRESS)
            .register(RepositoryService.class, repositoryService);
        binder.completionHandler(startFuture);
    }
}
